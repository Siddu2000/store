import * as React from "react";
import {useMediaQuery} from 'react-responsive'
// import { phone, tablet, laptop, desktop } from "./icons";

export default function Media() {
 const modileComp= useMediaQuery({query:"(max-width:375px)"})

  return (
    <section>
      <h1>useMediaQuery</h1>
      Resize your browser windows to see changes.
      <article>
        {
            modileComp && 
            
            <h1>Hello world</h1>
        }
      </article>
    </section>
  );
}
